/*
MIT License

Copyright (c) 2023 Greg King

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#include "fbdisplay.h"

#include <stdio.h>
#include <string.h>
#include <errno.h>


FbDisplay::FbDisplay() {
	m_Device = 0;
	m_Connector = NULL;
	m_Encoder = NULL;
	m_Crtc = NULL;
	
	m_Resolution = NULL;
	memset(&m_DumbBuffer, 0, sizeof(m_DumbBuffer));
	m_DumbBufferID = 0;
	m_DumbBufferPixelPtr = NULL;

	m_DrawBuffer = NULL;
	m_WidthInPixels = 0;
	m_HeightInPixels = 0;
	
	m_Scale = 1;
}

// initialises the display and displays a black screen
int FbDisplay::init() {
	int err = initDRM();
	if (err) {
		destroy();
		return err;
	}
		
	err = initDrawBuff();
	if (err) {
		destroy();
		return err;
	}
		
	return 0;
}

int FbDisplay::initDRM() {
	const char* const DEFAULT_DEVICE = "/dev/dri/card0";

	m_Device = open(DEFAULT_DEVICE, O_RDWR);
	if (m_Device < 0) {
		printf("Could not open dri device %s\n", DEFAULT_DEVICE);
		return -EINVAL;
	}
    
	drmModeResPtr res = drmModeGetResources(m_Device);
	if (!res) {
		printf("Could not get drm resources\n");
		return -EINVAL;
	}    

	// find first connector that is current and has a preferred resolution
	drmModeConnectorPtr connector = 0;
	for (int connIdx = 0; connIdx < res->count_connectors; connIdx++) {
		connector = drmModeGetConnectorCurrent(m_Device, res->connectors[connIdx]);
		if (!connector)
			continue;

		printf("Found connector: %p\n", (void*)connector);

		drmModeModeInfoPtr resolution = 0;
		m_Resolution = 0;
		int modeIdx = 0;
		while (m_Resolution == 0 && modeIdx < connector->count_modes) {
//		for (int modeIdx = 0; modeIdx < connector->count_modes; modeIdx++) {
			resolution = &connector->modes[modeIdx];
			printf("Found resolution %ux%u type:%x\n", resolution->hdisplay, resolution->vdisplay, resolution->type);
			printf("DRM_MODE_TYPE_PREFFERED=%x\n", DRM_MODE_TYPE_PREFERRED);
			if (resolution->type & DRM_MODE_TYPE_PREFERRED) {
				m_Resolution = resolution;
			} else {
				modeIdx++;
			}
		}
		
		if(m_Resolution) {
			m_Connector = connector;
			break;
		} else {
			drmModeFreeConnector(connector);
			connector = 0;
		}	
	}
	
	if (!m_Resolution || !m_Connector) {
		printf("Could not get connector with preferred resolution\n");
		return -EINVAL;
	}
	
	memset(&m_DumbBuffer, 0, sizeof(m_DumbBuffer));
	m_DumbBuffer.height = m_Resolution->vdisplay;
	m_DumbBuffer.width = m_Resolution->hdisplay;
	m_DumbBuffer.bpp = 16;

	int err = ioctl(m_Device, DRM_IOCTL_MODE_CREATE_DUMB, &m_DumbBuffer);
	if (err) {
		printf("Could not create dumb framebuffer (err=%d)\n", err);
		return -EINVAL;
	}

	err = drmModeAddFB(	m_Device, 
						m_Resolution->hdisplay, m_Resolution->vdisplay, 
						16, 16,
						m_DumbBuffer.pitch, 
						m_DumbBuffer.handle, 
						&m_DumbBufferID
					);
					
	if (err) {
		printf("Could not add framebuffer to drm (err=%d)\n", err);
        return -EINVAL;
    }

	// get the encoder
	m_Encoder = drmModeGetEncoder(m_Device, m_Connector->encoder_id);
	if (!m_Encoder) {
		printf("Could not get encoder\n");
		return -EINVAL;
    }

	// get the CRTC settings
	m_Crtc = drmModeGetCrtc(m_Device, m_Encoder->crtc_id);

	struct drm_mode_map_dumb mreq;

	memset(&mreq, 0, sizeof(mreq));
	mreq.handle = m_DumbBuffer.handle;

	err = drmIoctl(m_Device, DRM_IOCTL_MODE_MAP_DUMB, &mreq);
	if (err) {
		printf("Mode map dumb framebuffer failed (err=%d)\n", err);
		return -EINVAL;
	}

    m_DumbBufferPixelPtr = (uint16_t*)mmap(	0, m_DumbBuffer.size, 
											PROT_READ | PROT_WRITE, MAP_SHARED, 
											m_Device, 
											mreq.offset
				    					);
	if (m_DumbBufferPixelPtr == MAP_FAILED) {
		err = errno;
		printf("Mode map failed (err=%d)\n", err);
        return -EINVAL;
    }
    
	drmDropMaster(m_Device);

    return 0;
}

int FbDisplay::initDrawBuff() {
	const int DEFAULT_WIDTH=320;
	const int DEFAULT_HEIGHT=240;
	
	m_DrawBuffer = new uint8_t[DEFAULT_WIDTH*DEFAULT_HEIGHT];
	if (!m_DrawBuffer) {
		return -1;
	}
	memset(m_DrawBuffer, 0, DEFAULT_WIDTH*DEFAULT_HEIGHT);
	m_WidthInPixels = DEFAULT_WIDTH;
	m_HeightInPixels = DEFAULT_HEIGHT;
	
	memset(m_ColourTable, 0, sizeof(uint16_t)*MAX_COLOURS);
	m_ColourTable[1] = encodeColour(255, 255, 255);
	m_ColourTable[2] = encodeColour(255, 0, 0);	
	m_ColourTable[3] = encodeColour(0, 255, 0);	
	m_ColourTable[4] = encodeColour(0, 0, 255);	
	m_ColourTable[5] = encodeColour(255, 255, 0);	
	m_ColourTable[6] = encodeColour(0, 255, 255);	
	m_ColourTable[7] = encodeColour(255, 0, 255);	

	// if dstWidth/dstHeight < srcWidth/srcHeight we want to rotate drawbuffer render
	if (m_Resolution->hdisplay*m_HeightInPixels <
		m_WidthInPixels*m_Resolution->vdisplay 
		)
	{
		// rotated
		m_Rotated = true;
		int scaleH = m_Resolution->hdisplay / m_HeightInPixels;
		int scaleV = m_Resolution->vdisplay / m_WidthInPixels;
		m_Scale = (scaleH < scaleV) ? scaleH : scaleV;

		m_DstLeft = (m_Resolution->hdisplay - (m_HeightInPixels*m_Scale))/2;
		m_DstTop = (m_Resolution->vdisplay - (m_WidthInPixels*m_Scale))/2; 
	} else {
		m_Rotated = false;
		
		int scaleH = m_Resolution->hdisplay / m_WidthInPixels;
		int scaleV = m_Resolution->vdisplay / m_HeightInPixels;
		m_Scale = (scaleH < scaleV) ? scaleH : scaleV;
	
		m_DstLeft = (m_Resolution->hdisplay - (m_WidthInPixels*m_Scale))/2;
		m_DstTop = (m_Resolution->vdisplay - (m_HeightInPixels*m_Scale))/2; 
	}
	
	return 0;
}

// frees all allocated resources
void FbDisplay::destroy() {
	destroyDrawBuff();
	
	destroyDRM();
}

void FbDisplay::destroyDrawBuff() {
	if(m_DrawBuffer) {
		delete [] m_DrawBuffer;
		m_DrawBuffer = NULL;	
	}
	m_WidthInPixels = 0;
	m_HeightInPixels = 0;
}

void FbDisplay::destroyDRM() {
	drmSetMaster(m_Device);

	if(m_Encoder) {
		printf("Releasing encoder %p\n", (void*)m_Encoder);
		drmModeFreeEncoder(m_Encoder);
		m_Encoder = NULL;
	}
	if(m_DumbBufferID) {
		printf("Freeing dumb buffer ID\n");
		drmModeFreeFB(drmModeGetFB(m_Device, m_DumbBufferID));
		m_DumbBufferID = 0;
	}
	if (m_DumbBuffer.handle) {
		printf("Destroying Dumb Buffer\n");
		ioctl(m_Device, DRM_IOCTL_MODE_DESTROY_DUMB, m_DumbBuffer);
		memset(&m_DumbBuffer, 0, sizeof(m_DumbBuffer));
		m_DumbBufferPixelPtr = NULL;
	}
	if (m_Crtc) {
		printf("Freeing Crtc\n");
		/* Set back to original frame buffer */
		drmModeSetCrtc(	m_Device, 
						m_Crtc->crtc_id, 
						m_Crtc->buffer_id, 
						0, 0, 
						&m_Connector->connector_id, 
						1, 
						m_Resolution
					);
		drmModeFreeCrtc(m_Crtc);
		m_Crtc = NULL;
	}

	/* This will also release resolution */
	if (m_Connector) {
		printf("Freeing connector and resolution\n");
		drmModeFreeConnector(m_Connector);
		m_Connector = NULL;
		m_Resolution = 0;
	}

	drmDropMaster(m_Device);

	close(m_Device);	
	m_Device = 0;
}

void FbDisplay::dump() {
	printf("=================================================\n");
	printf("Device: %u\n", m_Device);
	printf("Connector: %p\n", (void*)m_Connector);
	printf("Encoder: %p\n", (void*)m_Encoder);
	printf("Crtc: %p\n", (void*)m_Crtc);
	
	printf("Resolution: %p\n", (void*)m_Resolution);
	printf("Dumb Buffer ID: %x\n", m_DumbBufferID);
	printf("DumbBuffer Pixel Ptr: %p\n", (void*)m_DumbBufferPixelPtr);
	
	printf("Draw Buffer Ptr: %p\n", (void*)m_DrawBuffer);
	printf("Width in Pixels: %u\n", m_WidthInPixels);
	printf("Height in Pixels: %u\n", m_HeightInPixels);

	printf("=================================================\n");
}

void FbDisplay::dumpColourTable() {
	for(int idx = 0; idx < MAX_COLOURS; idx+=16) {
		for(int subIdx = idx; subIdx < idx+16; subIdx++) { 
			printf("%d:%x\t", subIdx, m_ColourTable[subIdx]);
		}
		printf("\n");
	}
}

// returns a pointer to the top-left pixel of the buffer
uint8_t* 	FbDisplay::getDrawBuffer() {
	return m_DrawBuffer;
}

// returns the number of bytes in a row of the pixel buffer
uint16_t	FbDisplay::getBufferStride() {
	return m_WidthInPixels;	// just 256 colour at least for now
}

// return the dimensions of the pixel buffer
uint16_t	FbDisplay::getWidthInPixels() const {
	return m_WidthInPixels;
}

uint16_t	FbDisplay::getHeightInPixels() const {
	return m_HeightInPixels;
}

// rgb in range 0-255 even if only 5/6 bits used for display
void FbDisplay::setColour(uint8_t index, uint8_t r, uint8_t g, uint8_t b) {
	if(index < MAX_COLOURS) {
		m_ColourTable[index] = encodeColour(r, g, b);
	}
}

// generate an encoded 16-bit colour value from 8-bit channel values
uint16_t FbDisplay::encodeColour(uint8_t r, uint8_t g, uint8_t b) const {
	return (
			(((uint16_t)r >> 3) << 11) +
			(((uint16_t)g >> 2) << 5) +
			(((uint16_t)b >> 3) << 0)
		);
}

// set a range of colour values
void FbDisplay::setColourRange(	uint8_t firstIndex, 
								uint8_t numColours, 
								const uint16_t * colourArray 
							) 
{
	uint16_t start = firstIndex;
	uint16_t num = numColours;
	if (start+num > MAX_COLOURS) {
		num = MAX_COLOURS - start;
	}
	memcpy(m_ColourTable+start, colourArray, num*sizeof(uint16_t));
}

// makes the current pixel buffer visible on the main display.
// (colour table changes only take effect with this call)
void FbDisplay::presentDrawBuffer() {
	if( !drmSetMaster(m_Device) ) {
		if(m_Rotated) {
			presentDrawBufferRotated();	
		} else {
			presentDrawBufferNormal();
		}
	
		// set buffer on crtc
		drmModeSetCrtc(	m_Device, 
						m_Crtc->crtc_id, 
						m_DumbBufferID, 0, 0, 
						&m_Connector->connector_id, 
						1, m_Resolution
					);	
		
		drmDropMaster(m_Device);
	}
		
}

void FbDisplay::presentDrawBufferRotated() {
	uint16_t pitch = m_Resolution->hdisplay;
	uint16_t* start = m_DumbBufferPixelPtr;
	start += m_DstTop*pitch + m_DstLeft;
	uint16_t* end = start + m_WidthInPixels*m_Scale*pitch;
	
	uint16_t* column = new uint16_t[m_HeightInPixels*m_Scale];

	uint8_t* srcCol = m_DrawBuffer;
	
	while(start < end) {
		// generate column
		int dstIdx = 0;
		for(int srcIdx = (m_HeightInPixels-1)*m_WidthInPixels; srcIdx >= 0; srcIdx -= m_WidthInPixels) {
			for(int rpt = 0; rpt < m_Scale; rpt++) {
				column[dstIdx++] = m_ColourTable[srcCol[srcIdx]];
			}
		}
		
		for(int rpt = 0; rpt < m_Scale; rpt++) {
			memcpy(start, column, m_HeightInPixels*m_Scale*sizeof(uint16_t));
			start += pitch;
		}
		
		srcCol ++;
	}
	
	delete [] column;
}

void FbDisplay::presentDrawBufferNormal() {
	uint16_t pitch = m_Resolution->hdisplay;
	uint16_t* start = m_DumbBufferPixelPtr;
	start += m_DstTop*pitch + m_DstLeft;
	uint16_t* end = start + m_HeightInPixels*m_Scale*pitch;
	
	uint16_t* row = new uint16_t[m_WidthInPixels*m_Scale];
	
	uint8_t* srcRow = m_DrawBuffer;	
	
	while(start < end) {
		// generate row
		int dstIdx = 0;
		for(int srcIdx = 0; srcIdx < m_WidthInPixels; srcIdx++) {
			for(int rpt = 0; rpt < m_Scale; rpt++)
				row[dstIdx++] = m_ColourTable[srcRow[srcIdx]];
		}
		
		for(int rpt = 0; rpt < m_Scale; rpt++) {
			memcpy(start, row, m_WidthInPixels*m_Scale*sizeof(uint16_t));
			start += pitch;
		}
		
		srcRow += m_WidthInPixels;
	}	
	
	delete[] row;
}

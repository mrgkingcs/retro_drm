/*
MIT License

Copyright (c) 2023 Greg King

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#ifndef __FB_DISPLAY__
#define __FB_DISPLAY__

#include <cstdint>
#include <fcntl.h>
#include <unistd.h>

#include <sys/ioctl.h>
#include <sys/mman.h>

#include <libdrm/drm.h>
#include <libdrm/drm_mode.h>
#include <xf86drm.h>
#include <xf86drmMode.h>

using namespace std;

class FbDisplay {
private:
	static const int MAX_COLOURS = 256;

	int m_Device;
	drmModeConnectorPtr	m_Connector;
	drmModeEncoderPtr m_Encoder;
	drmModeCrtcPtr m_Crtc;
	
	drmModeModeInfoPtr m_Resolution;
	drm_mode_create_dumb m_DumbBuffer;
	uint32_t m_DumbBufferID;
	uint16_t* m_DumbBufferPixelPtr;

	uint8_t* m_DrawBuffer;
	uint16_t m_WidthInPixels;
	uint16_t m_HeightInPixels;
	
	uint16_t m_ColourTable[MAX_COLOURS];
	uint m_Scale;
	bool m_Rotated;
	uint m_DstLeft;
	uint m_DstTop;
	
private:
	int initDRM();
	int initDrawBuff();
	
	void destroyDRM();
	void destroyDrawBuff();

	void presentDrawBufferRotated();
	void presentDrawBufferNormal();

public:
	FbDisplay();
	
	// initialises the display and displays a black screen
	int init();
	
	// frees all allocated resources
	void destroy();
	
	void dump();
	void dumpColourTable();
	
	// returns a pointer to the top-left pixel of the buffer
	uint8_t* 	getDrawBuffer();
	
	// returns the number of bytes in a row of the pixel buffer
	uint16_t	getBufferStride();
	
	// return the dimensions of the pixel buffer
	uint16_t	getWidthInPixels() const;
	uint16_t	getHeightInPixels() const;
	
	// rgb in range 0-255 even if only 5/6 bits used for display
	void setColour(uint8_t index, uint8_t r, uint8_t g, uint8_t b);
	
	// generate an encoded 16-bit colour value from 8-bit channel values
	uint16_t encodeColour(uint8_t r, uint8_t g, uint8_t b) const;
	
	// set a range of colour values
	void setColourRange(uint8_t firstIndex, uint8_t numColours, const uint16_t * colourArray );

	// makes the current pixel buffer visible on the main display.
	// (colour table changes only take effect with this call)
	void presentDrawBuffer();
};

#endif
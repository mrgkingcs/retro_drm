/*
MIT License

Copyright (c) 2023 Greg King

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#include <cstdio>
#include <cstring>
#include <random>

#include "fbdisplay.h"

using namespace std;

int main(int argc, char**argv) {
	FbDisplay display;
	
	display.dump();	
	
	display.init();

	display.dump();	

	//display.dumpColourTable();
	//getchar();

	default_random_engine generator;
	uniform_int_distribution<int> distribution(0, 8);

	int numPixels = display.getWidthInPixels() * display.getHeightInPixels();
	
	for(int rpt = 0; rpt < 100; rpt++) {
		uint8_t* pix = display.getDrawBuffer();
		uint8_t* end = pix + numPixels;
		
		int bgCol = distribution(generator);
		int fgCol = (bgCol+1)%8;

		memset(pix, bgCol, numPixels);

		uint8_t* topLine = pix;		
		uint8_t* bottomLine = pix + display.getWidthInPixels()*(display.getHeightInPixels()-1);		
		for(int x = 0; x < display.getWidthInPixels(); x++) {
			topLine[x] = fgCol;
			bottomLine[x] = fgCol;
		}		
	
		uint8_t* leftLine = pix;
		uint8_t* rightLine = pix + display.getWidthInPixels() - 1;
		uint vertDelta = display.getWidthInPixels();

		uint8_t* diagLine = pix;
		uint delta = display.getWidthInPixels()+1;

		for(int count = 0; count < display.getHeightInPixels(); count++ ) {
			*(diagLine) = fgCol;
			diagLine += delta;
			
			*(leftLine) = fgCol;
			leftLine += vertDelta;
			
			*(rightLine) = fgCol;
			rightLine += vertDelta;
		}
		 
/*		while(pix < end) {
			int value = distribution(generator);
			*(pix++) = value;
		}	
*/		display.presentDrawBuffer();
	}
	
	getchar();	
			
	display.destroy();
	
	display.dump();	

	return 0;
}